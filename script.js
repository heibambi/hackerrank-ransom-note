function ransomeNote(magazineArr, noteArr) {
    let magazineMap = {}
    magazineArr.forEach( function (word) {
         if (magazineMap[word])
             magazineMap[word] = magazineMap[word] + 1;
         else
             magazineMap[word] = 1;
     })
     
    
    var result = true;
    noteArr.forEach(function (word) {
         if  (!magazineMap[word] || magazineMap[word] < 1)
             result = false;
         if (magazineMap[word] && magazineMap[word] > 0) {
             magazineMap[word] = magazineMap[word] - 1;
         }
     }) 
    
     return result;
}

const answer1 = ransomeNote(["give", "me", "one", "grand", "today", "night"] , ["give","one","grand","today"])
const answer2 = ransomeNote(["two", "times", "three", "is", "not", "four"] , ["two","times","two","is","four"])
const answer3 = ransomeNote(["ive", "got", "a", "lovely", "bunch", "of", "coconuts"], ["ive","got","some","coconuts"])

console.log({
    answer1,
    answer2,
    answer3
})